package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    HashMap<String,Double> bookPrices = new HashMap<>();

    double getBookPrice(String isbn) {
        bookPrices.put ("1", 10.0);
        bookPrices.put ("2", 45.0);
        bookPrices.put ("3", 20.0);
        bookPrices.put ("4", 35.0);
        bookPrices.put ("5", 50.0);
        return bookPrices.get(isbn);
    }
}